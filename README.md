# Docker tools

# Installation

In order to use this toolset, just use: 

```bash
make start
```

## Help

If you need support, just use:

```bash
make 
```

# Projects 

Once the tools are ready, time to access your applications, in order to do so:

```yaml
# Your docker-compose.override.yml 

## ...
  serviceName:
    labels:
      - "traefik.frontend.rule=Host:yourHost.docker"
    networks:
      - http

networks:
  http:
    external: true
```

Don't forget to create the `http` network:

```bash
docker network create http
```
