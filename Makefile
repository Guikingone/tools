DOCKER = docker
DOCKER_COMPOSE = docker-compose

##
## Project
## -------
##

.DEFAULT_GOAL := help

help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-20s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

install: ## Start the tools
install: start

start: ## Start the containers
start:
	$(DOCKER_COMPOSE) up -d

.PHONY: install start

##
## Traefik
## -------
##

a-logs: ## Allow to display the access logs (a specific file can be used via FILE)
a-logs:
	$(DOCKER_COMPOSE) exec traefik cat $(or $(FILE), /logs/core.log)

t-logs: ## Allow to display the Traefik logs
t-logs:
	$(DOCKER_COMPOSE) exec traefik cat /logs/traefik.log

.PHONY: start a-logs t-logs
